import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadExample implements Runnable {


    private AtomicInteger votes = new AtomicInteger(0);
    private String name;
    private int electoralVote;
    private int population;
    private int turnout;

    public ThreadExample(String name, int population, int electoralVote, int turnout) {

        this.name = name;
        this.electoralVote = electoralVote;
        this.population = population;

        Random random = new Random();
        this.turnout = random.nextInt(100);
    }

    public void run() {
        System.out.println("\n\nVotes for Candidate: \nState = " + name + " \nElectoral Votes = " + electoralVote
                + "\nPopulation = " + population + "\n");

        for(int count = 1; count < turnout; count++) {
                votes.incrementAndGet();
                System.out.print(name + " Vote totals: " + votes + "\n" + name + " is counting votes.\n");
                try {
                    Thread.sleep(electoralVote);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
        }
        System.out.println("\n\n" + name + " is done counting votes." + "\nVote total: " + votes);
    }
}
