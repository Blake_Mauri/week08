import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteThread {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(2);

        ThreadExample state1 = new ThreadExample("Arizona",800 , 11, 12);
        ThreadExample state2 = new ThreadExample("Colorado", 550, 8, 12);
        ThreadExample state3 = new ThreadExample("Utah", 700, 6, 11);
        ThreadExample state4 = new ThreadExample("Connecticut", 630, 5, 5);
        ThreadExample state5 = new ThreadExample("Florida", 1000, 28, 10);

        //creates the thread using a runnable class
        Thread runnableThread1 = new Thread(state1);
        Thread runnableThread2 = new Thread(state2);
        Thread runnableThread3 = new Thread(state3);

        //starts the runnable using an executor
        myService.execute(state1);
        myService.execute(state2);
        myService.execute(state3);
        myService.execute(state4);
        myService.execute(state5);

        //These start the runnable class directly
        runnableThread1.start();
        runnableThread2.start();
        runnableThread3.start();

        //shuts down the service
        myService.shutdown();
        System.out.println("Thread example complete. Ending Program");
    }

}
